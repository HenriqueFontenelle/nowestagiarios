##library(RMySQL) # will load DBI as well
library(RODBC)
library(data.table)
library("ggplot2")
normalize <- function(x) {
  resp <- ((x - min(x)) / (max(x) - min(x)))
  
  return (resp)
}

con <- odbcDriverConnect(
  "Driver={ODBC Driver 17 for SQL Server}; Server=tcp:10.10.0.10; Database=Olap-Reserva; UID=usr_r; Pwd=!@usr199421r#;"
)
## open a connection to a MySQL database
##con <-dbConnect(MySQL(), user ='lintpedidos', password = 'mdblsdh$$*#mndm', dbname = 'Olap-Reserva', host = '10.10.0.10')
## list the tables in the database
##dbListTables(con)
#userData <- dbGetQuery(con,'select * from dbo.TMP_LENTE_VENDA')
tempUserData <- sqlQuery(con, "select top 40000  cpf_cliente, count(cpf_cliente) as quantas_visitas, sum(valor_venda) as valor_gasto,DATEDIFF ( day , max(data_venda) , GETDATE()  ) as dias_sem_comprar
                      from TMP_LENTE_VENDA o
                      where valor_venda > 0 and cpf_cliente is not null and o.data_venda > '2019-01-01' and o.cpf_cliente <> '19100000000' and cpf_cliente <> '10464223000759' and cpf_cliente <> '11200418000401'     
                      group by cpf_cliente")

na.omit(tempUserData)

userData <- data.table()
userData$recencia <- tempUserData$dias_sem_comprar
userData$frequencia <- tempUserData$quantas_visitas
userData$monetario <- tempUserData$valor_gasto

normUserData <- data.table()
normUserData$recencia <- normalize(userData$recencia)
normUserData$frequencia <- normalize(userData$frequencia)
normUserData$monetario <- normalize(userData$monetario)

na.omit(normUserData)

weightedData <- data.table()
weightedData$recencia <- ((normUserData$recencia*(2/3))+(normUserData$monetario/3))
weightedData$frequencia <- ((normUserData$frequencia*(2/3))+(normUserData$monetario/3))

na.omit(weightedData)

pdf("UserDataPlots.pdf")

xLength <-range(min(userData$recencia),max(userData$recencia))
yLength <-range(min(userData$frequencia),max(userData$frequencia))

plot(x=userData$recencia, y=userData$frequencia, xlab = "recencia", ylab = "frequencia",xlim = rev(xLength),ylim = , main="User Data" ,pch=20 )
plot(x=normUserData$recencia, y=normUserData$frequencia, xlab = "recencia", ylab = "frequencia",xlim = c(1,0),main = "Normalized User Data",pch=20 )
plot(x=weightedData$recencia, y=weightedData$frequencia, xlab = "recencia", ylab = "frequencia",xlim = c(1,0),main = "Weighted User Data" , ylim = c(0,1), pch=20 )

dev.off()

odbcClose(con)

#dbDisconnect(con)